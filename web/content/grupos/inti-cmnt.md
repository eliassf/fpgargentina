---
denominacion: "Instituto Nacional de Tecnología Industrial (INTI) - Centro de Micro y Nanotecnologías (CMNT)"
categoria: "organismo"
sitio: "http://www.inti.gob.ar/areas/desarrollo-tecnologico-e-innovacion/areas-de-conocimiento/micro-y-nanotecnologias"
provincias: ["Buenos Aires"]
localidades: ["San Martín", "Bahía Blanca"]
fabricantes: [Xilinx, Intel, Microchip, Lattice]
dispositivos: [
  Spartan-2, Spartan-3, Spartan-6, SP601, LX9 MicroBoard, Virtex-2, Virtex-4, Virtex-5, Virtex-6, ML605, Zynq-7000, Zybo, ZC706, CIAA-ACC,
  Cyclone-V, DE10-nano, Cyclone-IV, DE0-nano, MAX-10, SmartFusion2, iCE40, iCEstick, Ice Breakout, EDU-CIAA-FPGA, "Placas Custom"
]
lenguajes: [VHDL, Verilog, Xilinx HLS]
herramientas: [ISE, Vivado, Vitis, "Vivado HLS", Quartus, Libero-SoC, IceCube2, Synplify, Modelsim]
opensource: [
  GHDL, iVerilog, Verilator, GtkWave,
  Yosys, next-pnr, icestorm, Symbiyosys,
  cocotb, PyFPGA
]
actualizado: "23-11-2020"
---

## Proyectos, productos o casos de uso

* Desarrollo de IP cores: USB, Ethernet, HDMI, FFT, High speed ADC, Dali, Tag RFID EPC.
* Empleo de bloques de hardware avanzados: Gigabit Transceivers, DDR, DMA.
* Realización de Testbenches con lenguajes VHDL, Verilog y Python (cocotb).
* Desarrollo de placas FPGA:
  * [s2proto](http://fpgalibre.sourceforge.net/hard.html#tp18) (Spartan-2)
  * [s3proto mini](http://fpgalibre.sourceforge.net/hard.html#tp22) (Spartan-3)
  * [CIAA-ACC](http://www.proyecto-ciaa.com.ar/devwiki/doku.php?id=desarrollo:ciaa_acc:ciaa_acc_inicio) (Zynq-7000)
  * [Kéfir](http://fpgalibre.sourceforge.net/Kefir/) (iCE40)
* Sistema FPGA de [QuADC](https://www.ptb.de/empir/quadc-project.html)
* Componente [Core-Comblock](https://gitlab.com/rodrigomelo9/core-comblock)

## Ofertas de capacitación

* Dictado de cursos sobre FPGA, VHDL y Verilog.
