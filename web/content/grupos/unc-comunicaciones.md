---
denominacion: "Universidad Nacional de Córdoba (UNC) - Facultad de Ciencias Exactas, Físicas y Naturales - Departamento de Computación - Laboratorio de Comunicaciones Digitales"
categoria: "universidad"
sitio: ""
provincias: ["Córdoba"]
localidades: ["Córdoba"]
fabricantes: [Xilinx, Intel]
actualizado: "21-11-2020"
---

La información de este grupo se encuentra incompleta, dado que no proporciono información oficial para los puntos faltantes.

## Proyectos, productos o casos de uso

* Implementación de algoritmos de procesamiento digital de señales para comunicaciones inalámbricas

## Ofertas de capacitación

* Materia Comunicaciones Digitales y Radios Cognitivas Definidas por Software


