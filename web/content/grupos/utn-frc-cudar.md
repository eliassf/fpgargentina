---
denominacion: "Universidad Tecnológica Nacional (UTN) - Facultad Regional Córdoba (FRC) - Centro Universitario de Desarrollo en Automación y Robótica (CUDAR)"
categoria: "universidad"
sitio: "https://www.institucional.frc.utn.edu.ar/internacionales/frc/investigacion.asp"
provincias: ["Córdoba"]
localidades: ["Córdoba"]
fabricantes: [Xilinx, Intel, Lattice]
dispositivos: [Basys, "Nexys 2", "Nexys 3", "Nexys 4", Virtex, DE1-SoC, Kefir]
lenguajes: [VHDL]
herramientas: [Vivado, ISE]
actualizado: "21-11-2020"
---

## Proyectos, productos o casos de uso

* Sistemas de control de lazo cerrado sobre motores de CC/CA

## Ofertas de capacitación

* Cursos de introducción Verilog/VHDL a nivel universitario
