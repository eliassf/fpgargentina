---
denominacion: "Jotatec"
categoria: "empresa"
sitio: "http://www.jotatec.com.ar/es/"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: ["Intel"]
dispositivos: ["Cyclone-III", "placas custom"]
lenguajes: [VHDL]
herramientas: [Quartus, Modelsim]
actualizado: "16-11-2020"
---

## Proyectos, productos o casos de uso

* Equipos de diagnóstico médico por ultrasonido (ecógrafos para cardiología).
