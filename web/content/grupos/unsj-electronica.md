---
denominacion: "Universidad Nacional de San Juan (UNSJ) - Laboratorio de Electrónica Digital"
categoria: "universidad"
sitio: "http://www.dea.unsj.edu.ar/sda"
provincias: ["San Juan"]
localidades: ["San Juan"]
fabricantes: [Xilinx, Intel, Microchip, Lattice]
dispositivos: [DE2-115, Nano, Nano SoC]
lenguajes: [VHDL, Verilog]
herramientas: [Quartus, Modelsim]
actualizado: "21-11-2020"
---


