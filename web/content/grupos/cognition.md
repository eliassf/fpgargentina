---
denominacion: "Cognition Business Inteligence"
categoria: "empresa"
sitio: "https://www.cognitionbi.com/"
provincias: ["Córdoba"]
localidades: ["Córdoba"]
fabricantes: [ "Xilinx"]
dispositivos: [Alveo, "AWS EC2 F1"]
lenguajes: [Verilog]
herramientas: [Vivado, Vitis]
actualizado: "16-11-2020"
---

> No proporcionó información oficial.
