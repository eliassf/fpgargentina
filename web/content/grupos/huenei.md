---
denominacion: "Huenei IT Services"
categoria: "empresa"
sitio: "https://www.huenei.com/"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: [ "Xilinx"]
dispositivos: [Alveo, "AWS EC2 F1"]
lenguajes: [VHDL, Verilog, "Xilinx HLS"]
herramientas: [Vivado, Vitis, "Vitis IA"]
actualizado: "16-11-2020"
---

## Proyectos, productos o casos de uso

* Machine / Deep Learning con Vitis IA.
* Procesamiento de imágenes y video.
* Sistemas de storage acelerado.
