---
denominacion: "BlueSphere Technologies"
categoria: "empresa"
sitio: "http://www.bluespheretech.com"
provincias: ["San Luis"]
localidades: ["San Luis"]
fabricantes: ["Xilinx", "Intel", "Microchip"]
dispositivos: [Arty, Artix-7, "DE0-Nano", "DE1-SoC", "placas custom"]
lenguajes: [VHDL, Verilog]
herramientas: [Vivado, Quartus, Libero-SoC]
opensource: [GHDL, iVerilog, GtkWave, cocotb, HDLmake]
actualizado: "23-11-2020"
---

## Proyectos, productos o casos de uso

* Plataforma de sensores de imágenes (Usando FPGA de Altera/Intel, placa custom).
* Comunicación USB3 - FPGA (Usando FPGA de Altera/Intel, placa custom).
* SoC para control de robot (Con placa Xilinx Arty A7)
* Serialización de datos para control de robot (Usando FPGA de Altera, DE-NanoSoC)

## Ofertas de capacitación

* Curso básico de VHDL.
* Curso básico de Verilog.
* Testbenchs con Python + CocoTB.
* Sistemas de Integración Continua para proyectos con FPGA / SoC.
