---
denominacion: "Universidad Nacional de Córdoba (UNC) - Facultad de Ciencias Exactas, Fisicas y Naturales - Departamento de Computación - Laboratorio de Arquitectura de Computadoras"
categoria: "universidad"
sitio: "http://www.efn.uncor.edu"
provincias: ["Córdoba"]
localidades: ["Córdoba"]
fabricantes: [Xilinx]
dispositivos: ["Basys 2", "Basys 3", "Nexys 3", "Nexys 4", Atlys]
lenguajes: [Verilog]
herramientas: [Vivado, ISE]
actualizado: "12-11-2020"
---

## Proyectos, productos o casos de uso

* Implementacion de procesador MIPS
* Procesador de Redes de Petri (en desarrollo)

## Ofertas de capacitación

* Materia de grado "Arquitectura de computadoras"

