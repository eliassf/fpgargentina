---
denominacion: "Comisión Nacional de Energía Atómica (CNEA) - Laboratorio Detección de Partículas y Radiación"
categoria: "organismo"
sitio: "http://labdpr.cab.cnea.gov.ar/"
provincias: ["Río Negro"]
localidades: ["Bariloche"]
fabricantes: [ "Xilinx"]
dispositivos: ["Red Pitaya", "Nexys 2", ZCU111]
lenguajes: [VHDL]
herramientas: [ISE, Vivado]
opensource: [GHDL]
actualizado: "10-11-2020"
---

## Proyectos, productos o casos de uso

* FIR Compiler, Bancos de filtros GDFT, FFT Core, DAQ.
