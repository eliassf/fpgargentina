---
denominacion: "Universidad Nacional de la Patagonia Austral (UNPA) -  Grupo de Investigación en Optoelectrónica Aplicada (GIOA)"
categoria: "universidad"
sitio: "https://www.unpa.edu.ar/cecyt/1876/grupo/gioa"
provincias: ["Santa Cruz"]
localidades: ["Río Gallegos"]
fabricantes: [Xilinx]
dispositivos: ["Arty", "Zynq-7000"]
lenguajes: [VHDL]
herramientas: [Vivado]
actualizado: "12-11-2020"
---

## Proyectos, productos o casos de uso

* Sistema de procesamiento de señales en sensores remotos

