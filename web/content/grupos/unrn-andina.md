---
denominacion: "Universidad Nacional de Río Negro (UNRN) - Centro Interdisciplinario de Telecomunicaciones, Electrónica, Computación y Ciencia Aplicada (CITECCA) - Sede Andina"
categoria: "universidad"
sitio: "http://www.unrn.edu.ar"
provincias: ["Río Negro"]
localidades: ["Bariloche"]
fabricantes: [Xilinx, Microchip]
dispositivos: [Zybo, Arty]
lenguajes: [VHDL, Verilog, "System Verilog"]
herramientas: [Vivado, Libero]
opensource: [RISC-V, verilator, gtkwave]
actualizado: "27-11-2020"
---

## Proyectos, productos o casos de uso

* Radar digital, Radar pasivo, Softcore RISC-V.

## Ofertas de capacitación

* Cursos de grado para Ingeniería Electrónica
