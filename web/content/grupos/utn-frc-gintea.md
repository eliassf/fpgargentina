---
denominacion: "Universidad Tecnológica Nacional (UTN) - Facultad Regional Córdoba (FRC) - Grupo de Investigación y Transferencia en Electrónica Avanzada (GINTEA)"
categoria: "universidad"
sitio: "https://www.investigacion.frc.utn.edu.ar/gintea/"
provincias: ["Córdoba"]
localidades: ["Córdoba"]
fabricantes: [ "Xilinx" ]
dispositivos: [Spartan, Pynq, ZedBoard,Zynq]
lenguajes: [VHDL]
herramientas: [Vivado, ISE]
actualizado: "21-11-2020"
---

## Proyectos, productos o casos de uso

* Packet Core in Mobile Networks ()FPRA-based Approach)

