---
denominacion: "Satellogic"
categoria: "empresa"
sitio: "http://www.satellogic.com"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: [Xilinx, Microchip, Lattice]
lenguajes: [Verilog, VHDL]
herramientas: [Vivado, "Libero-SoC"]
opensource: [nMigen, cocotb, iVerilog, verilator, GHDL, GTKwave, yosys]
actualizado: "17-11-2020"
---

> No declaran dispositivos.

## Proyectos, productos o casos de uso

* Satelites (payload, radios y procesamiento).
