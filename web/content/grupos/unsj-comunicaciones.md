---
denominacion: "Universidad Nacional de San Juan (UNSJ) - Laboratorio de Comunicaciones"
categoria: "universidad"
provincias: ["San Juan"]
localidades: ["San Juan"]
fabricantes: [ "Xilinx" ]
dispositivos: [ZedBoard]
lenguajes: [VHDL]
herramientas: [Vivado]
actualizado: "21-11-2020"
---

## Proyectos, productos o casos de uso

* Radio definida por software (SDR)

## Ofertas de capacitación

* Posibilidad de dictar cursos de posgrado en la temática de radio definida por software (hardware/software)
