---
denominacion: "Universidad de Buenos Aires - Facultad de Ingeniería (FIUBA)- Laboratorio de Sistemas Embebidos (LSE)"
categoria: "universidad"
sitio: "http://laboratorios.fi.uba.ar/lse/"
provincias: ["CABA"]
localidades: ["CABA"]
fabricantes: [Xilinx]
dispositivos: [ZedBoard, Arty, Zynq-7000, Spartan-7, Artix-7, "Nexys 2"]
lenguajes: [VHDL, Verilog, System Verilog, Xilinx HLS]
herramientas: [Vivado, ISE, Core Generator, Vivado HLS]
opensource: [ghdl, iverilog, gtkwave, hdlmake]
actualizado: "12-11-2020"
---

## Proyectos, productos o casos de uso

* Comunicaciones: QAM, CSS (Basado en LoRA), OFDM, emulación de canal
* Redes Neuronales: Procesadores de CNN
* DSP: Diferentes IPCores, FFT Radix 2 y 2^2 (iterativas y desenrolladas), convolución 2D, CORDIC, Descomposición QR, otros.
* Otros: Generación de números pseudo-aleatorios

## Ofertas de capacitación

* Capacitación In-Company
* Cursos relacionados con la Especialización y Maestría en Sistemas Embebidos (MSE/CESE)
