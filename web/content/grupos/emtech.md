---
denominacion: "Emtech SA"
categoria: "empresa"
sitio: "http://www.emtech.com.ar"
provincias: ["Buenos Aires", "Río Negro"]
localidades: ["Bahía Blanca", "Bariloche"]
fabricantes: [Xilinx, Intel, Microchip]
dispositivos: [Cyclone, Stratix, Spartan-6, UltraScale+, SmartFusion, RTG4, "Placas Custom"]
lenguajes: [VHDL, Verilog, "System Verilog", OpenCL]
herramientas: [ISE, Vivado, Quartus, "Libero-SoC", Questa, Modelsim, Synplify, Matlab, UVM]
actualizado: "17-11-2020"
---

## Proyectos, productos o casos de uso

* Radar y SDR, HPC, general purpose SoCs, aplicaciones espaciales, proposito general, glue logic.
* Experiencia con placas de Digilent, Avnet, Pentek, Bittware, Abacco, Ettus.

## Ofertas de capacitación

* Cursos a medida o como parte de curricula de Instituto Balseiro.
