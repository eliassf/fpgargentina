---
denominacion: "Tecnoaccion S.A."
categoria: "empresa"
sitio: "http://www.tecnoaccion.com.ar/"
provincias: ["Río Negro", "CABA"]
localidades: ["Bariloche", "CABA"]
fabricantes: [Xilinx]
dispositivos: ["Placas Custom"]
lenguajes: [VHDL]
herramientas: [Vivado, ISE]
actualizado: "27-11-2020"
---

## Proyectos, productos o casos de uso

* Terminal POS (TJ9) basada sólo en FPGA con softcore x86.
* Terminal POS (TJ10) Controladora de periféricos basada en FPGA.
