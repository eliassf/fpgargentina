---
denominacion: "Universidad Nacional de Mar del Plata (UNMdP) - Instituto de Investigaciones Científicas y Tecnológicas en Electrónica (ICyTE) - Laboratorio de Sistemas Caóticos"
categoria: "universidad"
provincias: ["Buenos Aires"]
localidades: ["Mar del Plata"]
fabricantes: [Xilinx, Intel]
dispositivos: [ZedBoard, Cyclone III, Cyclone IV]
lenguajes: [VHDL, Xilinx HLS]
herramientas: [Vivado, Vivado HLS]
opensource: [ghdl, iverilog, gtkwave, hdlmake]
actualizado: "12-11-2020"
---

## Proyectos, productos o casos de uso

* Aplicaciones para: bloque de ordenamiento, sistema caotico, exponente de Lyapunov
