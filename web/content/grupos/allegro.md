---
denominacion: "Allegro Microsystems Argentina"
categoria: "empresa"
asic: True
sitio: "https://www.allegromicro.com/en"
provincias: ["CABA"]
localidades: ["CABA"]
actualizado: "16-11-2020"
---

> Es empresa de desarrollo de ASIC. No proporcionó información oficial, pero poseen placas FPGA, probablemente para prototipado y pruebas de concepto.
