---
denominacion: "Universidad Nacional del Sur (UNS) - Laboratorio de Ingeniería de Computación (LICA) / Universidad Tecnológica Nacional (UTN) - Facultad Regional Bahía Blanca (FRBB)"
categoria: "universidad"
sitio: "http://www.lica.uns.edu.ar"
provincias: ["Buenos Aires"]
localidades: ["Bahía Blanca"]
fabricantes: [Intel]
dispositivos: [Cyclone-II, Cyclone-III, Cyclone-IV, Cyclone-V, DE2, DE2-115, DE1-SOC, DE10-Nano, MAX-10, Excalibur, Bemicro SDK]
lenguajes: [VHDL, Verilog]
herramientas: [Quartus, ModelSim]
opensource: [cocotb]
actualizado: "21-11-2020"
---

> Son grupos en dos universidades, con un mismo director, que a fines prácticos funcionan como un solo grupo según lo especificado.

## Proyectos, productos o casos de uso

* Simulador Radar: plataforma para la generación de escenarios artificiales para radares navales. Integra hardware para la generación de sincronismos y clutter y aplicación de Linux integrado en SoC para interface de usuario para configuración y generación gráfica de escenarios.
* Extractor Video Radar: plataforma para el procesamiento de video radar y extracción y seguimiento de móviles. Realizada mediante SoC de Intel integrando hardware de filtrado e inteface en Linux integrado para integración de estrategias de seguimiento.

## Ofertas de capacitación

* Cursos de grado (en ambas universidades FRBB-UTN y UNS) 
* Cursos de posgrados en el marco de la Especialización en "Tecnologías Digitales Configurables"
