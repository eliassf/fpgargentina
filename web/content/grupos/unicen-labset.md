---
denominacion: "Universidad Nacional del Centro de la Provincia de Buenos Aires (UNICEN) - Facultad de Ciencias Exactas - Laboratorio de Sistemas Embebidos Tandil (LabSET)"
categoria: "universidad"
sitio: "http://www.labset.exa.unicen.edu.ar"
provincias: ["Buenos Aires"]
localidades: ["Tandil"]
fabricantes: [ "Xilinx" ]
dispositivos: [Zybo, Pynq, Atlys, XUP-V2P, Virtex-5, Spartan-3, EDU-CIAA-FPGA]
lenguajes: [VHDL, Verilog, Xilinx HLS]
herramientas: [Vivado, ISE, Vivado HLS]
actualizado: "21-11-2020"
---

## Proyectos, productos o casos de uso

* IP Cores de aritmética basados en punto fijo/flotante
* Sistemas binarios/decimales (Suma, multiplicadores, raíz, logaritmo, entre otros)
* Desarrollos de arquitecturas RTL a medida (compresor de Imágenes ImageZero, Speckle)
* Desarrollos de conversores de protocolos de comunicación (ARINC429/Manchester/NMEA)
* Desarrollos de sistemas de visión basados en FPGA (AP SoC) (detección de malezas en agricultura, detección de fallas en líneas de producción de cerámicos)

## Ofertas de capacitación

* Cursos optativos para la carrera Ingeniería de Sistemas de UNICEN, que son válidos para los posgrados de Maestría en Ingeniería de Sistemas y Doctorado en Ciencias de la Computación: Diseño en Lógica Programable (actualmente denominado "Sistemas Embebidos en Lógica Programable") y "Programación Software de FPGAs" (HLS).
