---
title: "Acerca de"
---

# FPGArgentina

El relevamiento de grupos de desarrollo, capacidades y casos de uso de la tecnología FPGA en
Argentina, tiene como objetivo obtener y compartir un listado actualizado de los actores públicos
y privados, que participan en investigación y desarrollo, relacionado a la temática en el país.
La idea principal es conocernos un poco mejor como comunidad, saber quien tiene las mismas
inquietudes, detectar posibilidades de sinergia, identificar a quién se puede acudir por ayuda,
etc.

La información recolectada se encuentra disponible públicamente en este sitio y su
[repositorio](https://gitlab.com/rodrigomelo9/fpgargentina), bajo licencia
[Creative Commons Attribution 4.0 International](https://creativecommons.org/licenses/by/4.0)
(CC BY 4.0).

> **Descargo de responsabilidad:** este listado se distribuye con la intención de que resulte útil,
> pero sin ninguna garantía de exactitud o estado actualizado. Es principalmente resultado de la
> información recolectada, inicialmente mediante un formulario, y enriquecida con el tiempo.
> Si sos responsable del grupo, y no deseas que el mismo figure en el listado, contactate para que
> lo removamos.

El listado esta segmentado en 3 categorías:

* **Empresas:** deben poseer desarrollo o productos vigentes basado en la tecnología.
* **Organismos:** (Instituciones) con grupos de desarrollo activos.
* **Universidades:** (Facultades) con grupos o laboratorios de desarrollo activos.

> **Nota:** el relevamiento sólo incluye grupos, centros, institutos o laboratorios dedicados
> activamente a la temática FPGA, de manera sostenida en el tiempo. No incluimos cátedras,
> tanto de grado como de posgrado, salvo que además los docentes estén incluidos en la descripción
> anterior. Tampoco se consideran proyectos finales, estudiantes o autodidactas.
> El espíritu de esta decisión, es que figuren aquellos grupos de trabajo que forman
> regularmente recursos humanos a partir de trabajar con proyectos consolidados de I+D.

¿Hay información incorrecta o desactualizada?
¿Detectas que falta un grupo?
¿Alguno de los grupos ya no trabaja en la temática y debería ser retirado? 
En cualquier caso, podes abrir un [issue](https://gitlab.com/rodrigomelo9/fpgargentina/-/issues),
hacer el cambio necesario y solicitar un
[merge request](https://gitlab.com/rodrigomelo9/fpgargentina/-/merge_requests),
o [contactarnos](mailto:rodrigomelo9@gmail.com).

> Esta iniciativa es principalmente llevada adelante por
> [Rodrigo Alejandro Melo](https://www.linkedin.com/in/rodrigoalejandromelo/)
> (INTI, Centro de Micro y Nanotecnologías) y
> [Facundo Larosa](https://www.linkedin.com/in/facundo-santiago-larosa-200526a/)
> (UTN, Facultad Regional Haedo).
